package ru.fireburn.kittybox

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.json.JSONArray
import org.json.JSONObject
import org.junit.Assert

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import ru.fireburn.kittybox.ui.create.CreatePostActivity

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */

const val TEST_CONTENT = "Test content for Micropub"
val TEST_CATEGORIES = arrayOf("test", "category", "array", "should be", "concatenated")

@RunWith(AndroidJUnit4::class)
class CreatePostActivityInstrumentedTest {
    @Test
    fun testSimplePostSerialization() {
        val scenario = ActivityScenario.launch(CreatePostActivity::class.java)

        Espresso.onView(ViewMatchers.withId(R.id.content)).perform(ViewActions.typeText(TEST_CONTENT))

        scenario.onActivity {
            val jsonData = it.getMf2JsonData()
            assertNotNull(jsonData.optJSONObject("properties"))
            assertNotNull(jsonData.getJSONObject("properties").optJSONArray("content"))
            assertEquals(
                TEST_CONTENT,
                ((jsonData["properties"] as JSONObject)["content"] as JSONArray)[0]
            )
        }
    }

    @Test
    fun testCategoriesSerialization() {
        val scenario = ActivityScenario.launch(CreatePostActivity::class.java)

        Espresso.onView(ViewMatchers.withId(R.id.category)).perform(ViewActions.typeText(
            TEST_CATEGORIES.joinToString(", ")))

        scenario.onActivity {
            val jsonData = it.getMf2JsonData()
            assertNotNull(jsonData.optJSONObject("properties"))
            assertNotNull(jsonData.getJSONObject("properties").optJSONArray("category"))

            assertEquals(
                JSONArray(TEST_CATEGORIES),
                jsonData.getJSONObject("properties").optJSONArray("category")
            )
        }
    }
}
