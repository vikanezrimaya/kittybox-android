package ru.fireburn.kittybox;

import androidx.annotation.IntDef;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsIntent;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import android.net.Uri;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;

import ru.fireburn.kittybox.networking.KittyboxRequestQueue;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";

    protected static void updateEndpoints(SharedPreferences.Editor editor, EnumMap<Endpoints,String> endpointsMap) {
        editor.putString("me", endpointsMap.get(Endpoints.ME));
        editor.putString("authorization", endpointsMap.get(Endpoints.AUTHORIZATION));
        editor.putString("token", endpointsMap.get(Endpoints.TOKEN));
        editor.putString("micropub", endpointsMap.get(Endpoints.MICROPUB));
        editor.putString("microsub", endpointsMap.get(Endpoints.MICROSUB));
        editor.putString("media", endpointsMap.get(Endpoints.MEDIA));
    }
    private static void updateEndpoints(Context ctx, EnumMap<Endpoints, String> endpointsMap) {
        SharedPreferences pref = ctx.getSharedPreferences("endpoints", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        updateEndpoints(editor, endpointsMap);
        editor.apply();
    }
    public void performIndieAuth(String me, String uri, String responseType, @Nullable String scope) {
        Uri.Builder urlBuilder = Uri.parse(uri).buildUpon()
                .appendQueryParameter("me", me)
                .appendQueryParameter("redirect_uri", "kittybox://indieauth_callback")
                .appendQueryParameter("client_id", "https://kittybox.fireburn.ru/android")
                .appendQueryParameter("response_type", responseType);
        if (responseType.equals("code")) {
            urlBuilder.appendQueryParameter("scope", scope);
        }
        new CustomTabsIntent.Builder().build().launchUrl(this, urlBuilder.build());
    }
    protected void fetchIndieAuthToken(String code) {
        Toast.makeText(this, "Code was received!", Toast.LENGTH_SHORT).show();
        SharedPreferences pref = getSharedPreferences("endpoints", MODE_PRIVATE);
        String tokenEndpoint = pref.getString("token", "");
        RequestQueue queue = KittyboxRequestQueue.getQueue(this);
        StringRequest req = new StringRequest(
                Request.Method.POST, tokenEndpoint,
                response -> {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("access_token", (String) jsonObject.get("access_token"));
                        Toast.makeText(this,
                                "Got access token starting with "
                                        + ((String) jsonObject.get("access_token")).substring(0, 6),
                                Toast.LENGTH_LONG).show();
                        new DetermineEndpointsTask(endpointsMap -> {
                            updateEndpoints(editor, endpointsMap);
                            editor.apply();
                        }).execute((String) jsonObject.get("me"));
                        Intent intent = new Intent(this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } catch (JSONException e) {
                        Toast.makeText(this, "Something went wrong when extracting the access token from the response. " + e.getMessage(), Toast.LENGTH_LONG).show();
                        Log.e(TAG, "Current contents: " + response);
                    }
                }, error -> {
            Toast.makeText(this, "Something went wrong when exchanging the code for an access token. " + error.getMessage(), Toast.LENGTH_LONG).show();
            Log.e(TAG, "Error on token endpoint request: " + error.getMessage());
        }
        ) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> map = new HashMap<>(4);
                map.put("code", code);
                map.put("me", Objects.requireNonNull(pref.getString("me", null)));
                map.put("redirect_uri", "kittybox://indieauth_callback");
                map.put("client_id", "https://kittybox.fireburn.ru/android");
                Log.d(TAG, map.toString());
                return map;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> map = new HashMap<>();
                map.put("Accept", "application/json");
                return map;
            }
        };
        queue.add(req);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            Uri data = intent.getData();
            String code = Objects.requireNonNull(data).getQueryParameter("code");
            fetchIndieAuthToken(code);

        } catch (NullPointerException e) {
            // Nothing!
        }
    }

    private enum Endpoints {
        AUTHORIZATION, TOKEN, MICROPUB, MICROSUB, ME, MEDIA
    }

    private static class DetermineEndpointsTask extends AsyncTask<String, Void, EnumMap<Endpoints, String>> {
        final AsyncTaskCallback<EnumMap<Endpoints,String>> callback;
        DetermineEndpointsTask(AsyncTaskCallback<EnumMap<Endpoints,String>> callback) {
            this.callback = callback;
        }
        @Override
        protected @Nullable EnumMap<Endpoints,String> doInBackground(String... url) {
            boolean noMicropub = false;
            boolean noMicrosub = false;
            try {
                EnumMap<Endpoints, String> endpoints = new EnumMap<>(Endpoints.class);
                Document doc = Jsoup.connect(url[0]).get();
                // put "me" first - the url we got, the identity
                endpoints.put(Endpoints.ME, url[0]);
                try {
                    Element authEndpointLink = doc.select("link[rel=authorization_endpoint]").first();
                    endpoints.put(Endpoints.AUTHORIZATION, authEndpointLink.attributes().get("href"));
                } catch (NullPointerException e) {
                    return null;
                }
                try {
                    Element tokenEndpointLink = doc.select("link[rel=token_endpoint]").first();
                    endpoints.put(Endpoints.TOKEN, tokenEndpointLink.attributes().get("href"));
                } catch (NullPointerException e) {
                    return null;
                }
                try {
                    Element micropubLink = doc.select("link[rel=micropub]").first();
                    endpoints.put(Endpoints.MICROPUB, micropubLink.attributes().get("href"));
                } catch (NullPointerException e) {
                    // nothing - the app can work without it... but we set the flag
                    noMicropub = true;

                }
                try {
                    Element microsubLink = doc.select("link[rel=microsub]").first();
                    endpoints.put(Endpoints.MICROSUB, microsubLink.attributes().get("href"));
                } catch (NullPointerException e) {
                    noMicrosub = true;
                }
                try {
                    Element mediaLink = doc.select("link[rel=media_endpoint]").first();
                    endpoints.put(Endpoints.MEDIA, mediaLink.attributes().get("href"));
                } catch (NullPointerException e) {
                    // nothing
                }
                if (noMicropub && noMicrosub) {
                    // The app will be useless without both of these services.
                    return null;
                }
                return endpoints;
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(EnumMap<Endpoints, String> endpointsStringEnumMap) {
            callback.onTaskComplete(endpointsStringEnumMap);
        }
    }

    private interface AsyncTaskCallback<T> {
        void onTaskComplete(T object);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button loginButton = findViewById(R.id.login_button);
        loginButton.setOnClickListener(view -> {
            EditText urlField = findViewById(R.id.url);
            final String url = urlField.getText().toString();
            new DetermineEndpointsTask(endpointsMap -> {
                updateEndpoints(view.getContext(), endpointsMap);
                try {
                    performIndieAuth(url, endpointsMap.get(Endpoints.AUTHORIZATION),
                            "code",
                            "create update delete undelete media read follow mute block channels");
                } catch (NullPointerException e) {
                    Toast.makeText(view.getContext(),
                            "Something went wrong when determining required set of endpoints.",
                            Toast.LENGTH_LONG).show();
                }
            }).execute(url);
        });
    }
}
