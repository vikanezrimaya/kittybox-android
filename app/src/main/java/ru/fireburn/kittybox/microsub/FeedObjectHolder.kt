package ru.fireburn.kittybox.microsub

import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.json.JSONObject
import ru.fireburn.kittybox.R

open class FeedObjectHolder(private val view: View) : RecyclerView.ViewHolder(view) {
    open fun setData(it: JSONObject) {
        view.findViewById<TextView>(R.id.name).text = it.getString("url")
    }

    var obj: JSONObject? = null
        set(value) {
            Log.d("FeedObjectHolder", value?.toString())
            field = value
            value?.let {
                this.setData(it)
            }
        }
}