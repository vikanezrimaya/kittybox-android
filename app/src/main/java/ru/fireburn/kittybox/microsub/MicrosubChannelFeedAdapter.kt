package ru.fireburn.kittybox.microsub

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import ru.fireburn.kittybox.R

class MicrosubChannelFeedAdapter(private val ctx: Context)
    : PagedListAdapter<JSONObject, FeedObjectHolder>(DIFF_CALLBACK) {
    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<JSONObject>() {
            override fun areItemsTheSame(oldItem: JSONObject, newItem: JSONObject): Boolean {
                return oldItem["_id"] as String == newItem["_id"] as String
            }

            /**
             * Recursively compare JSONObjects.
             *
             * <ul>
             *   <li>JSONObjects are considered equal if all their contents are equal and they
             *   contain an equal number of keys.
             *   <li>JSONArrays are considered equal if their string representation are equal.
             *   <li>Any other objects use .equals() for comparison.
             * </ul>
             */
            override fun areContentsTheSame(oldItem: JSONObject, newItem: JSONObject): Boolean {
                if (oldItem.length() != newItem.length()) return false
                try {
                    for (key in oldItem.keys()) {
                        when (newItem.get(key)) {
                            is JSONObject -> if (!areContentsTheSame(oldItem.getJSONObject(key), newItem.getJSONObject(key))) return false
                            is JSONArray -> if (oldItem.getJSONArray(key).toString() != newItem.getJSONArray(key).toString()) return false
                            else -> if (oldItem.get(key) != newItem.get(key)) return false
                        }
                    }
                } catch (exc: JSONException) {
                    return false
                }
                return true
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous
        if (getItem(position) != null) {
            return when (getItem(position)!!.get("type") as String) {
                "entry" -> 1
                "card" -> 2
                else -> 0
            }
        } else {
            return -1 // placeholder
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedObjectHolder {
        val layout: View = LayoutInflater.from(
            ctx
        ).inflate(when (viewType) {
            1 -> R.layout.feed_entry_view
            2 -> R.layout.fragment_feed_object
            else -> R.layout.fragment_feed_object
        }, parent, false)
        return when (viewType) {
            1 -> FeedPostHolder(layout)
            2 -> FeedObjectHolder(layout)
            else -> FeedObjectHolder(layout)
        }
    }

    override fun onBindViewHolder(holder: FeedObjectHolder, position: Int) {
        Log.d(
            "MicrosubChannelAdapter",
            "binding an object to a view holder"
        )
        // will be null if it is a placeholder
        // beware of nulls
        holder.obj = getItem(position)
    }
}