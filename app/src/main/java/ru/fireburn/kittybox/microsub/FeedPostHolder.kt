package ru.fireburn.kittybox.microsub

import android.content.Context
import android.graphics.drawable.Drawable
import android.icu.text.SimpleDateFormat
import android.net.Uri
import android.text.Html
import android.text.format.DateUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.text.HtmlCompat.FROM_HTML_MODE_LEGACY
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import org.json.JSONArray
import org.json.JSONObject
import ru.fireburn.kittybox.R
import ru.fireburn.kittybox.networking.KittyboxRequestQueue
import ru.fireburn.kittybox.networking.MicropubJSONRequest
import java.util.*

class FeedPostHolder(private val view: View) : FeedObjectHolder(view) {
    inner class PhotoAttachmentViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        var photo: String? = null
            set(value) {
                field = value
                Glide.with(view)
                    .load(value).into(view.findViewById(R.id.photo))
            }
    }
    inner class PhotoAttachmentAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        var photos: Array<String>? = null
            set(value) {
                field = value
                notifyDataSetChanged()
            }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            val layout: View = LayoutInflater.from(parent.context).inflate(
                R.layout.photo_attachment_view, parent, false)
            return PhotoAttachmentViewHolder(layout)
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            Log.d(
                "PhotoAttachmentAdapter",
                "Attaching ${photos!![position]} to a view holder"
            )
            (holder as PhotoAttachmentViewHolder).photo = photos!![position]
        }

        override fun getItemCount(): Int {
            return photos?.size ?: 0
        }
    }
    override fun setData(it: JSONObject) {
        val nv: TextView = view.findViewById(R.id.name)
        if (it.has("name")) {
            nv.visibility = View.VISIBLE
            nv.text = it.getString("name")
        } else {
            nv.visibility = View.GONE
        }

        if (it.has("author")) {
            view.findViewById<View>(R.id.author).visibility =
                View.VISIBLE
            it.getJSONObject("author").let {
                if (it.isNull("name")) {
                    view.findViewById<View>(R.id.author_name).visibility =
                        View.GONE
                } else {
                    view.findViewById<TextView>(R.id.author_name).apply {
                        text = it.getString("name")
                        visibility = View.VISIBLE
                    }
                }
                view.findViewById<TextView>(R.id.author_url).text = it.getString("url")
                Log.d("FeedPostHolder", it.getString("photo") ?: "NO PHOTO HERE")
                val authorPhotoView = view.findViewById<ImageView>(R.id.author_photo)
                if (!it.isNull("photo")) {
                    Glide.with(view).load(it.getString("photo")).fitCenter()
                        .apply(RequestOptions.circleCropTransform())
                        .into(authorPhotoView)
                    authorPhotoView.visibility = View.VISIBLE
                } else {
                    authorPhotoView.visibility = View.GONE
                }
            }
        } else {
            view.findViewById<View>(R.id.author).visibility =
                View.GONE
        }

        if (it.has("published")) {
            try {
                val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault())
                val date = dateFormat.parse(it.getString("published"))
                view.findViewById<TextView>(R.id.published).text =
                    DateUtils.getRelativeDateTimeString(
                        view.context,
                        date.time,
                        DateUtils.MINUTE_IN_MILLIS,
                        DateUtils.WEEK_IN_MILLIS,
                        0
                    )
            } catch (e: Throwable) {
                Log.e("FeedPostHolder", e.toString(), e)
                view.findViewById<TextView>(R.id.published).text = it.getString("published")
            }
        } else {
            view.findViewById<View>(R.id.published).visibility =
                View.GONE
        }
        if (it.has("like-of") || it.has("in-reply-to") || it.has("bookmark-of") || it.has("repost-of")) {
            val url = it.getJSONArray(when {
                it.has("like-of") -> "like-of"
                it.has("bookmark-of") -> "bookmark-of"
                it.has("repost-of") -> "repost-of"
                else -> "in-reply-to"
            }).getString(0)
            view.findViewById<View>(R.id.reply_context).apply {
                val reactionDrawable: Int
                val reactionText: String
                when {
                    it.has("like-of") -> {
                        reactionDrawable = R.drawable.ic_favorite_black_24dp
                        reactionText = String.format(
                            context.getString(R.string.like_of),
                            generateMinimalReplyContext(url, context)
                        )
                    }
                    it.has("bookmark-of") -> {
                        reactionDrawable = R.drawable.ic_bookmark_black_24dp
                        reactionText = String.format(
                            context.getString(R.string.bookmark_of),
                            generateMinimalReplyContext(url, context)
                        )
                    }
                    it.has("repost-of") -> {
                        reactionDrawable = R.drawable.ic_repost_black_24dp
                        reactionText = String.format(
                            context.getString(R.string.repost_of),
                            generateMinimalReplyContext(url, context)
                        )
                    }
                    else -> {
                        reactionDrawable = R.drawable.ic_reply_black_24dp
                        reactionText = String.format(
                            context.getString(R.string.reply_to),
                            generateMinimalReplyContext(url, context)
                        )
                    }
                }
                visibility = View.VISIBLE
                this.findViewById<ImageView>(R.id.reply_context_icon).setImageDrawable(
                    this.resources.getDrawable(reactionDrawable, this.context.theme)
                )
                this.findViewById<TextView>(R.id.reply_context_text).text = reactionText
                setOnClickListener {
                    val builder = CustomTabsIntent.Builder();
                    builder.setToolbarColor(it.context.getColor(R.color.colorPrimary))
                    val customTabsIntent = builder.build();
                    customTabsIntent.launchUrl(it.context, Uri.parse(url))
                }
            }
        } else {
            view.findViewById<View>(R.id.reply_context).visibility = View.GONE
        }
        if (it.has("content")) {
            view.findViewById<TextView>(R.id.content).visibility =
                View.VISIBLE
            if (it.getJSONObject("content").has("html")) {
                view.findViewById<TextView>(R.id.content).text = Html.fromHtml(
                    it.getJSONObject("content").getString("html"),
                    FROM_HTML_MODE_LEGACY,
                    null, null
                )
            } else {
                view.findViewById<TextView>(R.id.content).text = it.getJSONObject("content")
                    .getString("text")
            }
        } else {
            view.findViewById<View>(R.id.content).visibility =
                View.GONE
        }
        if (it.has("photo")) {
            Log.d("FeedPostHolder", it.getJSONArray("photo").toString())
            val photoArray = it.getJSONArray("photo")
            view.findViewById<RecyclerView>(R.id.photo).apply {
                visibility = View.VISIBLE
                layoutManager = FlexboxLayoutManager(this.context).apply {
                    flexWrap = FlexWrap.WRAP
                    flexDirection = FlexDirection.ROW
                    alignItems = AlignItems.STRETCH
                }
                adapter = PhotoAttachmentAdapter().apply {
                    photos = Array(photoArray.length()) {
                        photoArray.getString(it)
                    }
                }
            }
        } else {
            view.findViewById<RecyclerView>(R.id.photo).apply {
                visibility = View.GONE
                adapter = null
            }
        }
        val checkinView = view.findViewById<View>(R.id.checkin)
        if (it.has("checkin")) {
            checkinView.visibility = View.VISIBLE
            val checkin = it.getJSONObject("checkin")
            checkinView.setOnClickListener {
                val url = checkin.getString("url")

                val builder = CustomTabsIntent.Builder();
                builder.setToolbarColor(it.context.getColor(R.color.colorPrimary));
                val customTabsIntent = builder.build();
                customTabsIntent.launchUrl(it.context, Uri.parse(url))
            }
            view.findViewById<TextView>(R.id.checkinText).text = view.resources.getString(R.string.checkin_at).format(checkin.getString("name"))
        } else {
            checkinView.visibility = View.GONE
        }
        val json = it
        view.findViewById<Button>(R.id.btn_like).apply {
            setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_border_black_24dp, 0, 0, 0)
            setOnClickListener {
                Log.d("LIKE_BTN", "Liking " + json.get("url"))
                it.setOnClickListener(null)
                (it as Button).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_black_24dp, 0, 0, 0)
                val sharedPrefs = it.context.getSharedPreferences("endpoints",
                    Context.MODE_PRIVATE
                );
                val syndicateTo = PreferenceManager.getDefaultSharedPreferences(it.context)
                    .getStringSet("default_syndication", null)
                val micropub = sharedPrefs.getString("micropub", null)
                val token = sharedPrefs.getString("access_token", null)
                KittyboxRequestQueue.getQueue(it.context)
                    .add(MicropubJSONRequest(
                        micropub, token,
                        JSONObject().apply {
                            this.put("type", JSONArray().apply { this.put("h-entry") })
                            this.put("properties", JSONObject().apply {
                                this.put(
                                    "like-of",
                                    JSONArray().apply { this.put(json.getString("url")) })
                                syndicateTo?.let {
                                    this.put("mp-syndicate-to", JSONArray(it))
                                }
                            })
                        }, fun(location: String) {
                            Toast.makeText(
                                it.context,
                                "Liked post, see $location",
                                Toast.LENGTH_SHORT
                            ).show()
                            Log.d("LIKE_BTN", "Liked the post, location: $location")
                        }, fun(err: VolleyError) {
                            // XXX be more specific!!!
                            /* This kind of errors is a code smell to me. I'll leave it here for
                             * debugging, but seriously, leaving this kind of a thing in PRODUCTION
                             * code is something one should not do.
                             */
                            it.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_border_black_24dp, 0, 0, 0)
                            Toast.makeText(
                                it.context,
                                "Liking post failed. Please check your internet connection and try again.",
                                Toast.LENGTH_SHORT
                            ).show()
                            Log.d("LIKE_BTN", err.toString())
                        }
                    ).setRetryPolicy(DefaultRetryPolicy(
                        // Some Micropub servers are REALLY slow.
                        // This may cause likes to be duplicated.
                        // Not everyone's server is super fast like Kittybox in my testing environment,
                        // which I found the hard way by testing in production (where I have a super-slow
                        // old version of the backend which is completely SYNCHRONOUS, including Webmentions.
                        // No threading, no concurrency at all. Old-style...
                        // Let's change the default timeout by octupling it from 2.5s to 20s...
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 8,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)))
            }
            view.findViewById<ImageButton>(R.id.btn_more).setOnClickListener {
                val url = json.getString("url")

                val builder = CustomTabsIntent.Builder();
                val customTabsIntent = builder.build();
                builder.setToolbarColor(it.context.getColor(R.color.colorPrimary));
                customTabsIntent.launchUrl(it.context, Uri.parse(url));
            }
        }
    }

    companion object {
        @JvmStatic
        fun generateMinimalReplyContext(uriString: String?, context: Context): Any? {
            val uri: Uri = Uri.parse(uriString)
            return when {
                // minimal reply context: https://indieweb.org/reply-context#Minimal_text_reply_contexts
                uri.host == "twitter.com" -> String.format(context.getString(R.string.user_s_tweet), uri.pathSegments[0])
                uri.host == "github.com" && uri.pathSegments[2] == "issues" -> String.format(
                    context.getString(R.string.github_issue_no_of_project),
                    uri.pathSegments[3],
                    uri.pathSegments[0],
                    uri.pathSegments[1]
                )
                else -> uri.toString()
            }
        }
    }
}
