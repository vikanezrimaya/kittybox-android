package ru.fireburn.kittybox.microsub

import android.app.Application
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.fireburn.kittybox.R

open class MicrosubChannelFragment : Fragment() {

    companion object {
        fun newInstance(uid: String) = MicrosubChannelFragment().apply {
            arguments = Bundle().apply {
                putString("uid", uid)
            }
        }
    }

    protected open val uid: String
        get() = arguments?.getString("uid")!!

    private lateinit var viewModel: MicrosubChannelViewModel
    private lateinit var root: View
    private lateinit var viewTracker: ViewTracker

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        root = inflater.inflate(R.layout.microsub_channel_fragment, container, false)
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val prefs = context!!.getSharedPreferences("endpoints", Application.MODE_PRIVATE)
        val microsubUri = Uri.parse(prefs.getString("microsub", null))
        val accessToken = prefs.getString("access_token", null)!!

        viewModel = ViewModelProvider(this, viewModelFactory {
            MicrosubChannelViewModel(microsubUri, accessToken, uid)
        }).get(MicrosubChannelViewModel::class.java)

        val feedAdapter = MicrosubChannelFeedAdapter(context!!)
        viewModel.objects.observe(this, Observer {
            feedAdapter.submitList(it)
            Log.d("MicrosubChannelFragment", "updated observed values!")
            Log.d("MicrosubChannelFragment", it.toString())
        })

        //viewTracker = ViewTracker(
            root.findViewById<RecyclerView>(R.id.feed).also {
                it.layoutManager = LinearLayoutManager(context!!)
                it.adapter = feedAdapter
            }
        //)
        //viewTracker.startTracking()
    }

    //override fun onStop() {
    //    super.onStop()
    //    viewTracker.stopTracking()
    //}

    @Suppress("UNCHECKED_CAST")
    inline fun <VM : ViewModel> viewModelFactory(crossinline f: () -> VM) =
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(aClass: Class<T>):T = f() as T
        }
}

