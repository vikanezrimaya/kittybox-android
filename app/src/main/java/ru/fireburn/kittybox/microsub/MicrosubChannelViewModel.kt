package ru.fireburn.kittybox.microsub

import android.net.Uri
import android.util.Log
import androidx.lifecycle.*
import androidx.paging.*
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.authentication
import com.github.kittinunf.fuel.json.responseJson
import org.json.JSONArray
import org.json.JSONObject

class MicrosubPostDataSource(
    private val microsubUri: Uri, private val accessToken: String, private val uid: String
) : PageKeyedDataSource<String, JSONObject>() {

    val uriBuilder = microsubUri.buildUpon()
            .appendQueryParameter("action", "timeline")
            .appendQueryParameter("channel", uid)

    class Factory(private val microsubUri: Uri, private val accessToken: String, private val uid: String) : DataSource.Factory<String, JSONObject>() {
        val sourceLiveData = MutableLiveData<MicrosubPostDataSource>()

        override fun create(): DataSource<String, JSONObject> {
            return MicrosubPostDataSource(microsubUri, accessToken, uid).also {
                sourceLiveData.postValue(it)
            }

        }
    }

    companion object {
        private const val TAG = "MicropubPostDataSource"
    }

    override fun loadBefore(params: LoadParams<String>, callback: LoadCallback<String, JSONObject>) {
        val uri = uriBuilder
            .appendQueryParameter("limit", params.requestedLoadSize.toString())
            .appendQueryParameter("before", params.key).toString()
        Fuel.get(uri).authentication().bearer(accessToken).also {
            Log.d(TAG, it.toString())
        }.responseJson { request, response, result ->
            val json = result.get().obj()
            Log.d(TAG, json.opt("paging").toString())
            val objects: JSONArray = (json["items"] as JSONArray)
            val list: ArrayList<JSONObject> = ArrayList();
            for (i in 0 until objects.length()) {
                val channel: JSONObject = objects[i] as JSONObject
                list.add(channel)
            }
            try {
                val paging = json["paging"] as JSONObject
                if (paging.optString("before") == params.key) {
                    callback.onResult(listOf(), null)
                } else callback.onResult(
                    list as List<JSONObject>, paging.optString("before")
                )
            } catch (exc: ClassCastException) {
                Log.d(TAG, "in before: Encountered ClassCastException. Number of items: ${list.size}, pageKey = null")
                callback.onResult(list as List<JSONObject>, null)
            }
        }
    }

    override fun loadInitial(params: LoadInitialParams<String>, callback: LoadInitialCallback<String, JSONObject>) {
        val uri = uriBuilder.appendQueryParameter("limit", params.requestedLoadSize.toString()).toString()
        Fuel.get(uri).authentication().bearer(accessToken).also {
            Log.d(TAG, it.toString())
        }.responseJson { request, response, result ->
            val json = result.get().obj()
            Log.d(TAG, json.opt("paging").toString())
            val objects: JSONArray = (json["items"] as JSONArray)
            val list: ArrayList<JSONObject> = ArrayList();
            for (i in 0 until objects.length()) {
                val channel: JSONObject = objects[i] as JSONObject
                list.add(channel)
            }
            try {
                val paging = json["paging"] as JSONObject
                var after = paging.optString("after")
                // Sniff the next page. If the after token is the same, discard the current token.
                // We're at the end...
                Fuel.get(uriBuilder
                    .appendQueryParameter("limit", params.requestedLoadSize.toString())
                    .appendQueryParameter("after", after).toString())
                    .authentication().bearer(accessToken)
                    .responseJson().third.component1()?.obj()?.optJSONObject("paging")?.let {
                    if (it.optString("after") == null || it.getString("after") == after) {
                        after = null
                    }
                }
                callback.onResult(list as List<JSONObject>, null, after)
            } catch (exc: ClassCastException) {
                Log.d(TAG, "in after: Encountered ClassCastException. Number of items: ${list.size}, pageKey = null")
                callback.onResult(list as List<JSONObject>, null, null)
            }
        }
    }

    override fun loadAfter(params: LoadParams<String>, callback: LoadCallback<String, JSONObject>) {
        val uri = uriBuilder
            .appendQueryParameter("limit", params.requestedLoadSize.toString())
            .appendQueryParameter("after", params.key).toString()
        Fuel.get(uri).authentication().bearer(accessToken).also {
            Log.d(TAG, it.toString())
        }.responseJson { request, response, result ->
            val json = result.get().obj()
            Log.d(TAG, json.opt("paging").toString())
            val objects: JSONArray = (json["items"] as JSONArray)
            val list: ArrayList<JSONObject> = ArrayList();
            for (i in 0 until objects.length()) {
                val channel: JSONObject = objects[i] as JSONObject
                list.add(channel)
            }
            try {
                val paging = json["paging"] as JSONObject
                if (paging.optString("after") == params.key) {
                    callback.onResult(list as List<JSONObject>, null)
                } else {
                    var after = paging.optString("after")
                    // Sniff the next page. If the after token is the same, discard the current token.
                    // We're at the end...
                    Fuel.get(uriBuilder
                        .appendQueryParameter("limit", params.requestedLoadSize.toString())
                        .appendQueryParameter("after", after).toString())
                        .authentication().bearer(accessToken)
                        .responseJson().third.component1()?.obj()?.optJSONObject("paging")?.let {
                        if (it.optString("after") == null || it.getString("after") == after) {
                            after = null
                        }
                    }

                    callback.onResult(list as List<JSONObject>, after)
                }
            } catch (exc: ClassCastException) {
                Log.d(TAG, "in after: Encountered ClassCastException. Number of items: ${list.size}, pageKey = null")
                callback.onResult(list as List<JSONObject>, null)
            }
        }
    }

}

class MicrosubChannelViewModel(microsubUri: Uri, accessToken: String, uid: String) : ViewModel() {
    val objects: LiveData<PagedList<JSONObject>> = MicrosubPostDataSource.Factory(
        microsubUri, accessToken, uid
    )
        .map {
            // TODO inject reply contexts
            return@map it
        }
        .toLiveData(Config(5, 15, false))
}
