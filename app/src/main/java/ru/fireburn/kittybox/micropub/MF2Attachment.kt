package ru.fireburn.kittybox.micropub

data class MF2Attachment(val type: String, val data: Any)