package ru.fireburn.kittybox
import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.preference.PreferenceManager
import org.osmdroid.config.Configuration
import java.io.File

class Application : Application() {
    companion object {
        const val UPLOAD_SERVICE_NOTIFICATION_CHANNEL = "MediaUploadService"
    }

    private fun createUploadServiceNotificationChannel() {
        if (Build.VERSION.SDK_INT >= 26) {
            val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(NotificationChannel(
                UPLOAD_SERVICE_NOTIFICATION_CHANNEL,
                "Media Uploads",
                NotificationManager.IMPORTANCE_LOW
            ))

        }
    }

    override fun onCreate() {
        super.onCreate()

        createUploadServiceNotificationChannel()

        // initialize OSMDroid
        Configuration.getInstance().apply {
            load(
                applicationContext, PreferenceManager.getDefaultSharedPreferences(applicationContext)
            )
            // create a cache folder in real cache directory
            osmdroidTileCache = File(cacheDir, "osmdroid").also { it.mkdirs() }
        }

    }

}