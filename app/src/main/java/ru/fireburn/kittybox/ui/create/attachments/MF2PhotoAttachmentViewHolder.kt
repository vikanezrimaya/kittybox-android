package ru.fireburn.kittybox.ui.create.attachments

import android.util.Log
import android.view.View
import com.bumptech.glide.Glide
import ru.fireburn.kittybox.R
import ru.fireburn.kittybox.micropub.MF2Attachment

class MF2PhotoAttachmentViewHolder(v: View) : MF2AttachmentViewHolder(v) {
    override var attachment: MF2Attachment? = null
        set(item) {
            Log.d("MF2PhotoAttachmentViewHolder", item.toString())
            item?.data?.let {
                Glide.with(view).load(it as String).into(view.findViewById(R.id.photo))
            }
            field = item
        }
}