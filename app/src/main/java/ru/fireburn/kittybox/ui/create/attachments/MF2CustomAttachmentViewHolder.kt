package ru.fireburn.kittybox.ui.create.attachments

import android.util.Log
import android.view.View
import android.widget.TextView
import org.json.JSONException
import org.json.JSONObject
import ru.fireburn.kittybox.R
import ru.fireburn.kittybox.micropub.MF2Attachment
import java.lang.ClassCastException

class MF2CustomAttachmentViewHolder(v: View) : MF2AttachmentViewHolder(v) {
    override var attachment: MF2Attachment? = null
        set(item) {
            item?.let {
                view.findViewById<TextView>(R.id.attachmentType).text = it.type
                try {
                    Log.d("MF2AttachmentViewHolder", "${it.data}")
                    Log.d("MF2AttachmentViewHolder", it.data.javaClass.toString())
                    view.findViewById<TextView>(R.id.jsonData).text =
                        JSONObject(it.data as String).toString(2)
                } catch (exc: JSONException) {
                    view.findViewById<TextView>(R.id.jsonData).text = it.data.toString()
                }
            }
            field = item
        }
}