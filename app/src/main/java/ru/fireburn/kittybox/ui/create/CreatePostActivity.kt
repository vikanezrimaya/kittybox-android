package ru.fireburn.kittybox.ui.create

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.net.toFile
import androidx.core.text.trimmedLength
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.fireburn.kittybox.R

import kotlinx.android.synthetic.main.activity_create_post.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import ru.fireburn.kittybox.micropub.MF2Attachment
import ru.fireburn.kittybox.ui.create.attachments.AttachmentsAdapter

class CreatePostActivity : MicropubActivity() {
    override val TAG = "CreatePostActivity"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_post)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        attachmentsAdapter = AttachmentsAdapter(this)
        findViewById<RecyclerView>(R.id.attachments).apply {
            adapter = attachmentsAdapter
            setHasFixedSize(false)
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.HORIZONTAL))
        }
        if (savedInstanceState == null) intent?.let {
            findViewById<TextView>(R.id.content).text = intent.getStringExtra(Intent.EXTRA_TEXT) ?: ""
            intent.getStringExtra(INTENT_EXTRA_MF2_JSON)?.let {
                hydrateMf2JsonData(JSONObject(it))
            }
            intent.type?.let {
                Log.d(TAG, "Received intent $intent")
                when {
                    it.startsWith("image/") -> {
                        Log.d(TAG, "${intent.extras}")
                        intent.getParcelableExtra<Uri>(Intent.EXTRA_STREAM)?.let { uri ->
                            Log.d(TAG, "Uploading image $uri")
                            contentResolver.query(uri, null, null, null, null)?.use { cursor ->
                                val sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE)
                                cursor.moveToFirst()
                                uploadAttachment(
                                    "photo",
                                    contentResolver.openInputStream(uri) ?: return,
                                    cursor.getLong(sizeIndex),
                                    contentResolver.getType(uri) ?: return
                                )
                            }
                        }
                    }
                    else -> {}
                }
            }
        }
        savedInstanceState?.let {
            hydrateMf2JsonData(JSONObject(it.getString("mf2")))
            it.getString("currentPhotoPath")?.let {
                currentPhotoPath = it
            }
        }
        findViewById<Button>(R.id.submit).setOnClickListener(this::submitPost)
        findViewById<Button>(R.id.photo).setOnClickListener(this::showAttachPhotoDialog)
        findViewById<Button>(R.id.checkin).setOnClickListener(this::attachCheckin)
    }


    override fun onFinishedUpload(id: String, url: String) {
        if (pendingUploads.isEmpty()) findViewById<Button>(R.id.submit).isEnabled = true
    }

    override fun hydrateMf2JsonData(json: JSONObject) {
        val props = json.getJSONObject("properties")
        if (props.has("name")) findViewById<TextView>(R.id.name).text =
            props.getJSONArray("name").getString(0) ?: ""
        if (props.has("content")) findViewById<TextView>(R.id.content).text =
            props.getJSONArray("content").getString(0) ?: ""
        if (props.has("category")) findViewById<TextView>(R.id.category).text =
            props.getJSONArray("category").join(", ") ?: ""
        val propsWithFields = arrayOf("name", "content", "category", "mp-syndicate-to")
        for (prop in props.keys()) {
            if (prop in propsWithFields) continue
            Log.d(TAG, "adding $prop with content ${props.opt(prop)}")
            props.optJSONArray(prop)?.let { array ->
                for (i in 0 until array.length()) {
                    attachmentsAdapter.add(MF2Attachment(prop, array.getString(i)))
                }
            }
        }
    }

    override fun getMf2JsonData(): JSONObject {
        val name = findViewById<TextView>(R.id.name).text.toString()
        val content = findViewById<TextView>(R.id.content).text.toString()
        val category: List<String> = findViewById<TextView>(R.id.category).text.toString()
            .split(Regex(" *, *"))
            .filter { it != "" }
        val props = JSONObject().apply {
            if (name.trimmedLength() > 0) this.put("name", wrapInMf2Array(name))
            if (content.trimmedLength() > 0) this.put("content", wrapInMf2Array(content))
            if (category.isNotEmpty()) this.put("category", JSONArray(category))
            val syndicateTo = PreferenceManager.getDefaultSharedPreferences(this@CreatePostActivity)
                .getStringSet("default_syndication", null)
            Log.d("CreatePostActivity", syndicateTo?.toString() ?: "null")
            if (syndicateTo?.size ?: 0 > 0) this.put("mp-syndicate-to", JSONArray(syndicateTo))
            // attachments
            for (i in attachmentsAdapter.list) {
                Log.d("CreatePostActivity", "Saving: $i")
                // Kotlin weirdness, it thinks my JSONObject is a string...
                try {
                    if (this.optJSONArray(i.type) == null) {
                        this.put(i.type, wrapInMf2Array(JSONObject(i.data as String)))
                    } else {
                        this.getJSONArray(i.type).put(JSONObject(i.data as String))
                    }
                } catch (exc: JSONException) {
                    if (this.optJSONArray(i.type) == null) {
                        this.put(i.type, wrapInMf2Array(i.data as String))
                    } else {
                        this.getJSONArray(i.type).put(i.data as String)
                    }
                }
            }
        }
        return JSONObject().apply {
            this.put("type", wrapInMf2Array("h-entry"))
            this.put("properties", props)
        }
    }
}