package ru.fireburn.kittybox.ui.create

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.core.text.trimmedLength
import ru.fireburn.kittybox.R

import kotlinx.android.synthetic.main.activity_create_venue.toolbar
import org.json.JSONArray
import org.json.JSONObject
import ru.fireburn.kittybox.micropub.MF2Attachment
import ru.fireburn.kittybox.ui.create.attachments.ChooseGeoActivity
import ru.fireburn.kittybox.ui.create.attachments.AttachmentsAdapter

class CreateVenueActivity : MicropubActivity() {
    override val TAG = "CreateVenueActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_venue)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        attachmentsAdapter =
            AttachmentsAdapter(this)
        savedInstanceState?.let {
            hydrateMf2JsonData(JSONObject(it.getString("mf2")))
            it.getString("currentPhotoPath")?.let {
                currentPhotoPath = it
            }
        }
        findViewById<Button>(R.id.geo).setOnClickListener {
            startActivityForResult(Intent(this, ChooseGeoActivity::class.java), CODE_GEO_PICKED)
        }
    }

    override fun getMf2JsonData(): JSONObject {
        val name = findViewById<TextView>(R.id.name).text.toString()
        val note = findViewById<TextView>(R.id.note).text.toString()
        val slug = findViewById<TextView>(R.id.slug).text.toString()
        val props = JSONObject().apply {
            if (name.trimmedLength() > 0) this.put("name", wrapInMf2Array(name))
            if (note.trimmedLength() > 0) this.put("note", wrapInMf2Array(note))
            if (slug.trimmedLength() > 0) this.accumulate("mp-slug", slug)
            // attachments
            for (i in attachmentsAdapter.list) {
                Log.d("CreatePostActivity", i.toString())
                if (!this.has(i.type)) {
                    this.put(i.type, JSONArray())
                }
                this.getJSONArray(i.type).put(i.data)
            }
        }
        return JSONObject().apply {
            this.put("type", wrapInMf2Array("h-card"))
            this.put("properties", props)
        }
    }

    override fun hydrateMf2JsonData(json: JSONObject) {
        val props = json.getJSONObject("properties")
        if (props.has("name")) findViewById<TextView>(R.id.name).text =
            props.getJSONArray("name").getString(0) ?: ""
        if (props.has("note")) findViewById<TextView>(R.id.note).text =
            props.getJSONArray("note").getString(0) ?: ""
        if (props.has("mp-slug")) findViewById<TextView>(R.id.slug).text =
            props.getJSONArray("mp-slug").getString(0) ?: ""
        val additionalProps = arrayOf("photo", "geo")
        for (prop in additionalProps) {
            props.optJSONArray(prop)?.let { array ->
                for (i in 0 until array.length()) {
                    attachmentsAdapter.add(MF2Attachment(prop, array.getString(i)))
                }
            }
        }
    }

    override fun onFinishedUpload(id: String, url: String) {
        if (pendingUploads.isEmpty()) findViewById<Button>(R.id.submit).isEnabled = true
    }
}
