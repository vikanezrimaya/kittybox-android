package ru.fireburn.kittybox.ui.create.attachments

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.toBitmap
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.compass.CompassOverlay
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay
import ru.fireburn.kittybox.R
import java.io.File


class ChooseGeoActivity : AppCompatActivity() {

    companion object {
        const val REQUEST_LOCATION_PERMISSION = 0
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_choose_geo)
        findViewById<MapView>(R.id.map).apply {
            setTileSource(TileSourceFactory.MAPNIK)
            zoomController.setVisibility(CustomZoomButtonsController.Visibility.SHOW_AND_FADEOUT)
            setMultiTouchControls(true)
        }
        centerMap()
    }

    private fun centerMap() {
        findViewById<MapView>(R.id.map).apply {
            controller.apply {
                setZoom(15.0)
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_LOCATION_PERMISSION)
                    return
                }
                val location = (applicationContext.getSystemService(LOCATION_SERVICE) as LocationManager).getLastKnownLocation(LocationManager.GPS_PROVIDER)
                setCenter(GeoPoint(location.latitude, location.longitude))
            }
            val locationOverlay = MyLocationNewOverlay(GpsMyLocationProvider(context), this)
            locationOverlay.enableMyLocation()
            locationOverlay.setPersonIcon(context.getDrawable(R.drawable.marker_default).toBitmap())
            val compassOverlay = CompassOverlay(context, InternalCompassOrientationProvider(context), this)
            overlays.add(locationOverlay)
            overlays.add(compassOverlay)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_LOCATION_PERMISSION && permissions.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            centerMap()
        } else {
            if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                AlertDialog.Builder(this).apply {
                    setMessage("Location permission is required to search for your location. This app respects your privacy and doesn't transmit location data anywhere besides your Micropub endpoint.")
                    setPositiveButton("OK") { dialogInterface, _ ->
                        dialogInterface.dismiss()
                        centerMap()
                    }
                    setNegativeButton("Cancel") { dialogInterface, _ ->
                        dialogInterface.dismiss()
                        this@ChooseGeoActivity.setResult(Activity.RESULT_CANCELED)
                        this@ChooseGeoActivity.finish()
                    }
                    show()
                }
            } else {
                this@ChooseGeoActivity.setResult(Activity.RESULT_CANCELED)
                this@ChooseGeoActivity.finish()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        val prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        findViewById<MapView>(R.id.map).onResume() //needed for compass, my location overlays, v6.0.0 and up
    }
    override fun onPause() {
        super.onPause()
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        val prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration.getInstance().save(this, prefs);
        findViewById<MapView>(R.id.map).onPause() //needed for compass, my location overlays, v6.0.0 and up
    }
}
