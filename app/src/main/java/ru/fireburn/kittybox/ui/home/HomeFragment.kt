package ru.fireburn.kittybox.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import ru.fireburn.kittybox.microsub.MicrosubChannelFragment
import ru.fireburn.kittybox.R
import ru.fireburn.kittybox.ui.create.CreatePostActivity

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        val tabLayout: TabLayout = root.findViewById(R.id.tabLayout)
        val viewPager: ViewPager = root.findViewById(R.id.channelPager)
        val channelPagerAdapter = ChannelPagerAdapter(childFragmentManager);
        viewPager.adapter = channelPagerAdapter
        tabLayout.setupWithViewPager(viewPager)
        homeViewModel.channels.observe(this, Observer {
            channelPagerAdapter.items = it
        })
        root.findViewById<FloatingActionButton>(R.id.createPostButton).setOnClickListener {
            startActivity(Intent(it.context, CreatePostActivity::class.java))
        }
        return root
    }
    class ChannelPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        override fun getItem(position: Int): Fragment {
            return MicrosubChannelFragment.newInstance(items!!.get(position).uid)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return items!!.get(position).name
        }

        override fun getCount(): Int {
            return (items?.lastIndex?:-1)+1
        }
        var items: List<MicrosubChannel>? = null
            set(value) {
                field = value
                notifyDataSetChanged()
            }
    }
}
