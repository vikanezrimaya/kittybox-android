package ru.fireburn.kittybox.ui.create.attachments

import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import com.github.kittinunf.fuel.Fuel
import org.jsoup.Jsoup
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.DefaultOverlayManager
import org.osmdroid.views.overlay.TilesOverlay
import ru.fireburn.kittybox.R
import ru.fireburn.kittybox.micropub.MF2Attachment

class StaticMapOverlayManager(tilesOverlay: TilesOverlay) : DefaultOverlayManager(tilesOverlay) {
    override fun onDoubleTap(e: MotionEvent?, pMapView: MapView?): Boolean {
        return true
    }

    override fun onDoubleTapEvent(e: MotionEvent?, pMapView: MapView?): Boolean {
        return true
    }
}

class MF2CheckinAttachmentViewHolder(v: View) : MF2AttachmentViewHolder(v) {
    override var attachment: MF2Attachment? = null
        set(item) {
            item?.let {
                view.findViewById<TextView>(R.id.checkinText).text = String.format(view.resources.getString(
                    R.string.checkin_at
                ), it.data)
                view.findViewById<MapView>(R.id.minimap).apply {
                    setTileSource(TileSourceFactory.MAPNIK)
                    view.setHasTransientState(true)
                    isFlingEnabled = false
                    isEnabled = false
                    isClickable = false
                    isFocusable = false
                    setOnTouchListener { v, event -> true }
                    setOnClickListener { v ->  }
                    setMultiTouchControls(false)
                    overlayManager = StaticMapOverlayManager(TilesOverlay(tileProvider, context))
                    zoomController.setVisibility(CustomZoomButtonsController.Visibility.NEVER)
                    Fuel.get(it.data as String).responseString { request, response, result ->
                        val parsed = Jsoup.parse(result.component1(), it.data)
                        val hcard = parsed.body().getElementsByClass("h-card")[0]
                        val geo = hcard.getElementsByClass("h-geo")[0]
                        view.findViewById<TextView>(R.id.checkinText).text = String.format(view.resources.getString(
                            R.string.checkin_at
                        ), hcard.getElementsByClass("p-name")[0].text())
                        val lat = geo.getElementsByClass("p-latitude")[0]
                        val lon = geo.getElementsByClass("p-longitude")[0]
                        controller.apply {
                            setZoom(17.0)
                            try {
                                setCenter(GeoPoint(lat.data().toDouble(), lon.data().toDouble()))
                            } catch (exc: NumberFormatException) {
                                setCenter(GeoPoint(lat.text().toDouble(), lon.text().toDouble()))
                            }
                        }
                    }

                }
            }
            field = item
        }
}