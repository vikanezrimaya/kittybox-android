package ru.fireburn.kittybox.ui.create

import android.app.Activity
import android.content.*
import android.net.Uri
import android.os.Bundle
import android.os.IBinder
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.annotation.CallSuper
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.core.net.toFile
import com.android.volley.DefaultRetryPolicy
import com.android.volley.VolleyError
import org.json.JSONArray
import org.json.JSONObject
import ru.fireburn.kittybox.R
import ru.fireburn.kittybox.micropub.MF2Attachment
import ru.fireburn.kittybox.networking.KittyboxRequestQueue
import ru.fireburn.kittybox.networking.MediaUploadBroadcastReceiver
import ru.fireburn.kittybox.networking.MediaUploadService
import ru.fireburn.kittybox.networking.MicropubJSONRequest
import ru.fireburn.kittybox.ui.create.attachments.AttachmentsAdapter
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.io.Serializable
import java.util.*
import kotlin.collections.HashMap

abstract class MicropubActivity : AppCompatActivity() {
    companion object {
        @JvmStatic
        protected fun wrapInMf2Array(obj: Any): JSONArray {
            return JSONArray().apply {
                this.put(obj)
            }
        }
        internal const val CODE_IMAGE_PICKED = 1
        internal const val CODE_IMAGE_CAPTURED = 2
        internal const val CODE_CHECKIN_PICKED = 6
        internal const val CODE_GEO_PICKED = 7
        const val INTENT_EXTRA_MF2_JSON = "ru.fireburn.kittybox.intent.EXTRA_MF2_JSON"
    }
    protected open val TAG = "MicropubActivity"
    protected val pendingUploads: MutableMap<String, String> = HashMap()
    lateinit var attachmentsAdapter: AttachmentsAdapter
    protected lateinit var uploadService: MediaUploadService
    protected var boundToUploadService = false
    protected val connection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            boundToUploadService = false
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            uploadService = (service as MediaUploadService.MediaUploadBinder).getService()
            boundToUploadService = true
        }

    }
    protected lateinit var currentPhotoPath: String
    protected val broadcastReceiver = object : MediaUploadBroadcastReceiver() {
        override fun onProgress(id: String, readBytes: Long, totalBytes: Long) {
            // Do nothing for now. In the future I could delegate this to a method in subclass.
        }

        override fun onFinished(id: String, url: String) {
            pendingUploads[id]?.let {
                attachmentsAdapter.add(MF2Attachment(it, url))
                pendingUploads.remove(id)
                this@MicropubActivity.onFinishedUpload(id, url)
            }
        }
    }

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val filter = IntentFilter().apply {
            addAction(MediaUploadService.MEDIA_UPLOAD_PROGRESS)
            addAction(MediaUploadService.MEDIA_UPLOAD_FINISHED)
        }
        registerReceiver(broadcastReceiver, filter)
        savedInstanceState?.let {
            @Suppress("UNCHECKED_CAST")
            pendingUploads.putAll(it.getSerializable("uploads") as Map<String, String>)
        }
        bindService(Intent(this, MediaUploadService::class.java), connection, Context.BIND_AUTO_CREATE)
    }

    @CallSuper
    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReceiver)
        unbindService(connection)
        boundToUploadService = false
    }

    abstract fun getMf2JsonData(): JSONObject
    abstract fun hydrateMf2JsonData(json: JSONObject)
    abstract fun onFinishedUpload(id: String, url: String)

    @CallSuper
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putString("mf2", getMf2JsonData().toString())
        if (::currentPhotoPath.isInitialized) {
            outState.putString("currentPhotoPath", currentPhotoPath)
        }
        outState.putSerializable("uploads", pendingUploads as Serializable)
    }

    protected fun attachCheckin(it: View) {
        val intent = Intent(it.context, SelectVenueActivity::class.java)
        startActivityForResult(intent, CODE_CHECKIN_PICKED)
    }

    protected fun showAttachPhotoDialog(it: View) {
        AlertDialog.Builder(it.context)
            .setPositiveButton("Take photo") { dialogInterface, which ->
                dialogInterface.dismiss()
                attachPhotoFromCamera(it)
            }
            .setNeutralButton("Choose photo") { dialogInterface, which ->
                dialogInterface.dismiss()
                attachPhotoFromChooser(it)
            }
            .setNegativeButton("Cancel") { dialogInterface, which ->
                dialogInterface.dismiss()
            }
            .setCancelable(true)
            .setMessage("Select the source of a photo")
            .setTitle("Attach photo")
            .show()
    }

    protected fun attachPhotoFromChooser(it: View) {

    }

    protected fun attachPhotoFromCamera(it: View) {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                val photoFile: File? = try {
                    val path = this.cacheDir.resolve("uploads")
                    if (!path.exists()) path.mkdir()
                    File.createTempFile(
                        UUID.randomUUID().toString(), ".jpg",
                        path
                    ).apply {
                        currentPhotoPath = absolutePath
                        Log.d("CreatePostActivity", currentPhotoPath)
                    }
                } catch (exc: IOException) {
                    Toast.makeText(this, "Couldn't create a temporary file.", Toast.LENGTH_LONG)
                        .show()
                    Log.e("CreatePostActivity", Log.getStackTraceString(exc))
                    null
                }
                photoFile?.also {
                    val photoURI: Uri =
                        FileProvider.getUriForFile(
                            this,
                            "ru.fireburn.kittybox.fileprovider",
                            it
                        )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, CODE_IMAGE_CAPTURED)
                }
            }
        }
    }

    protected fun submitPost(v: View) {
        val jsonData = getMf2JsonData()
        Log.d("CreatePostActivity", jsonData.toString(2))
        val sharedPrefs = this.getSharedPreferences("endpoints", Context.MODE_PRIVATE);
        val micropub = sharedPrefs.getString("micropub", null)
        val token = sharedPrefs.getString("access_token", null)
        KittyboxRequestQueue.getQueue(this)
            .add(MicropubJSONRequest(
                micropub, token,
                jsonData, fun(location: String) {
                    Log.d(TAG, "Posted, location: $location")
                    setResult(Activity.RESULT_OK, Intent().apply {
                        putExtra("uri", location)
                    })
                    finish()
                }, fun(err: VolleyError) {
                    // XXX be more specific!!!
                    /* This kind of errors is a code smell to me. I'll leave it here for
                     * debugging, but seriously, leaving this kind of a thing in PRODUCTION
                     * code is something one should not do.
                     */
                    Toast.makeText(
                        this,
                        "Sending post failed. Please check your internet connection and try again.",
                        Toast.LENGTH_LONG
                    ).show()
                    Log.e(TAG, err.toString(), err)
                }
            ).setRetryPolicy(
                DefaultRetryPolicy(
                    // Some Micropub servers are REALLY slow.
                    // This may cause likes to be duplicated.
                    // Not everyone's server is super fast like Kittybox in my testing environment,
                    // which I found the hard way by testing in production.
                    // Let's change the default timeout by increasing it from 2.5s to 20s...
                    DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 8,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                )
            ))

    }

    protected fun uploadAttachment(type: String, file: File, mime: String? = null) {
        return uploadAttachment(type, file.inputStream(), file.length(), mime)
    }

    protected fun uploadAttachment(type: String, inputStream: InputStream, size: Long, mime: String? = null) {
        if (boundToUploadService) {
            pendingUploads.put(uploadService.uploadFile(inputStream, size, mime), type)
            findViewById<Button>(R.id.submit).isEnabled = pendingUploads.isEmpty()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                CODE_IMAGE_PICKED -> {
                    val imageUri = data?.data ?: return
                    // Upload file to media endpoint and set it as an attachment
                    uploadAttachment("photo", imageUri.toFile(), null)
                }
                CODE_IMAGE_CAPTURED -> {
                    // Upload file to media endpoint and set it as an attachment
                    uploadAttachment("photo", File(currentPhotoPath), null)
                }
                CODE_CHECKIN_PICKED -> {
                    Log.d(TAG, data.toString())
                    val checkinLocation = data?.getStringExtra("uri") ?: return
                    // Add a check-in attachment
                    attachmentsAdapter.add(
                        MF2Attachment(
                            "checkin",
                            checkinLocation
                        )
                    )
                }
            }
        }
    }
}