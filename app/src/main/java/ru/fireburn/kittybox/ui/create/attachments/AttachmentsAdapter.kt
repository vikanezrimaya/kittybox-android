package ru.fireburn.kittybox.ui.create.attachments

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.fireburn.kittybox.R
import ru.fireburn.kittybox.micropub.MF2Attachment

class AttachmentsAdapter(private val ctx: Context) : RecyclerView.Adapter<MF2AttachmentViewHolder>() {
    val list: List<MF2Attachment> =
        ArrayList(1)
    fun add(attachment: MF2Attachment): Boolean {
        return (list as ArrayList).add(attachment).also {
            notifyDataSetChanged()
        }
    }
    fun removeAt(index: Int): MF2Attachment {
        return (list as ArrayList).removeAt(index).also {
            notifyDataSetChanged()
        }
    }
    fun remove(attachment: MF2Attachment): Boolean {
        return (list as ArrayList).remove(attachment).also {
            notifyDataSetChanged()
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (list[position].type) {
            "photo"   -> 1
            "video"   -> 2
            "audio"   -> 3
            "geo"     -> 4
            "checkin" -> 5
            else      -> 0
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MF2AttachmentViewHolder {
        val layout: View = LayoutInflater.from(
            ctx
        ).inflate(when (viewType) {
            1 -> R.layout.creator_photo_attachment
            5 -> R.layout.creator_checkin_attachment
            else -> R.layout.creator_custom_attachment
        }, parent, false)
        return when (viewType) {
            1 -> MF2PhotoAttachmentViewHolder(layout)
            5 -> MF2CheckinAttachmentViewHolder(layout)
            else -> MF2CustomAttachmentViewHolder(layout)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MF2AttachmentViewHolder, position: Int) {
        holder.attachment = list[position]
    }

}