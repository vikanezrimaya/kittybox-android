package ru.fireburn.kittybox.ui.notifications

import ru.fireburn.kittybox.microsub.MicrosubChannelFragment

class NotificationsFragment : MicrosubChannelFragment() {
    override val uid = "notifications"
}