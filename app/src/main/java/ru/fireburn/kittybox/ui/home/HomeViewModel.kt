package ru.fireburn.kittybox.ui.home

import android.app.Application
import android.net.Uri
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.volley.RequestQueue
import org.json.JSONArray
import org.json.JSONObject
import ru.fireburn.kittybox.networking.AuthenticatedJsonObjectRequest
import ru.fireburn.kittybox.networking.KittyboxRequestQueue

class HomeViewModel(application: Application) : AndroidViewModel(application) {
    private val requestQueue: RequestQueue = KittyboxRequestQueue.getQueue(application.applicationContext)
    private val microsubUri: Uri
    private val accessToken: String?

    init {
        val prefs = application.getSharedPreferences("endpoints", Application.MODE_PRIVATE)
        microsubUri = Uri.parse(prefs.getString("microsub", null))
        accessToken = prefs.getString("access_token", null)
    }
    private val _channels: MutableLiveData<List<MicrosubChannel>> by lazy {
        MutableLiveData<List<MicrosubChannel>>().also {
            loadChannels()
        }
    }
    val channels: LiveData<List<MicrosubChannel>> = _channels

    private fun loadChannels() {
        val request = AuthenticatedJsonObjectRequest(
            microsubUri.buildUpon().appendQueryParameter("action", "channels").toString(),
            accessToken,
            fun(json: JSONObject) {
                val channels: JSONArray = (json["channels"] as JSONArray)
                val list: ArrayList<MicrosubChannel> = ArrayList();
                for (i in 0 until channels.length()) {
                    val channel: JSONObject = channels[i] as JSONObject
                    if (channel["uid"] == "notifications") continue
                    list.add(MicrosubChannel(channel["name"] as String, channel["uid"] as String))
                }
                _channels.value = list
            }, null
        )
        requestQueue.add(request)
    }
}