package ru.fireburn.kittybox.ui.create

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_select_venue.*
import ru.fireburn.kittybox.R
import ru.fireburn.kittybox.networking.AuthenticatedJsonObjectRequest
import ru.fireburn.kittybox.networking.KittyboxRequestQueue


class SelectVenueActivity : AppCompatActivity() {
    companion object {
        const val REQUEST_LOCATION_PERMISSION = 0
        const val CODE_CREATE_CHECKIN = 0
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_venue)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener {
            startActivityForResult(Intent(this, CreateVenueActivity::class.java), CODE_CREATE_CHECKIN)
        }

        tryRequestLocation()
    }
    private fun tryRequestLocation() {
        if (checkSelfPermission(ACCESS_FINE_LOCATION) == PERMISSION_GRANTED) {
            requestLocation()
        } else {
            // App doesn't have access to the device's location. Make a request for permission.
            requestPermissions(arrayOf(ACCESS_FINE_LOCATION), REQUEST_LOCATION_PERMISSION)
        }
    }

    /**
     * Request location from Google Play Services if available, fall back to LocationManager if not.
     * Then use a callback to hydrate the UI with received locations.
     *
     * <p>This function is wrapped by {@link #tryRequestLocation tryRequestLocation()} so permissions are checked.
     * */
    @SuppressLint("MissingPermission")
    private fun requestLocation() {
        //if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this) == SUCCESS) {
        //    val fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        //    fusedLocationClient.lastLocation.addOnSuccessListener(::onLocationReceived).addOnFailureListener {
        //        Snackbar.make(findViewById(R.id.coordinatorLayout), "Can't find your location. Try again later.", Snackbar.LENGTH_INDEFINITE).show()
        //    }
        //} else {
        onLocationReceived((applicationContext.getSystemService(LOCATION_SERVICE) as LocationManager).getLastKnownLocation(LocationManager.GPS_PROVIDER))
        //}
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_LOCATION_PERMISSION && permissions.isNotEmpty() && grantResults[0] == PERMISSION_GRANTED) {
            requestLocation()
        } else {
            if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
                AlertDialog.Builder(this).apply {
                    setMessage("Location permission is required to search for nearby venues. This app respects your privacy and doesn't transmit location data anywhere besides your Micropub endpoint.")
                    setPositiveButton("OK") { dialogInterface, _ ->
                        dialogInterface.dismiss()
                        tryRequestLocation()
                    }
                    setNegativeButton("Cancel") { dialogInterface, _ ->
                        dialogInterface.dismiss()
                        this@SelectVenueActivity.setResult(Activity.RESULT_CANCELED)
                        this@SelectVenueActivity.finish()
                    }
                    show()
                }
            } else {
                this@SelectVenueActivity.setResult(Activity.RESULT_CANCELED)
                this@SelectVenueActivity.finish()
            }
        }
    }
    private fun onLocationReceived(geo: Location) {
        Log.d("SelectVenueActivity", geo.toString())
        Log.d("SelectVenueActivity", "${geo.latitude}, ${geo.longitude}, ${geo.altitude}, ${geo.accuracy}")
        val sharedPrefs = this.getSharedPreferences("endpoints", Context.MODE_PRIVATE)
        val requestQueue = KittyboxRequestQueue.getQueue(this)
        requestQueue.add(AuthenticatedJsonObjectRequest(
            Uri.parse(sharedPrefs.getString("micropub", null)).buildUpon()
                .appendQueryParameter("q", "geo")
                .appendQueryParameter("lat", geo.latitude.toString())
                .appendQueryParameter("lon", geo.longitude.toString())
                .build().toString(),
            sharedPrefs.getString("access_token", null),
            {
                val venueCard = findViewById<View>(R.id.includeVenueCard)
                val venue = it.getJSONObject("geo")
                venueCard.findViewById<TextView>(R.id.name).text = venue["label"] as String
                venueCard.findViewById<TextView>(R.id.geo).text = "${venue["latitude"]}, ${venue["longitude"]}"
                venueCard.setOnClickListener {
                    Log.d("SelectVenueActivity", "onClickListener lololol")
                    val uri = venue.optString("url", "geo:${venue["latitude"]},${venue["longitude"]};name=${venue["label"]}")
                    setResult(Activity.RESULT_OK, Intent().apply {
                        putExtra("uri", uri)
                    })
                    finish()
                }
                Log.d("SelectVenueActivity", "${venueCard.hasOnClickListeners()}")
            },
            {
                Snackbar.make(findViewById(R.id.coordinatorLayout), "Can't find venues nearby due to a server error.", Snackbar.LENGTH_INDEFINITE).show()
            }
        ))
    }
}
