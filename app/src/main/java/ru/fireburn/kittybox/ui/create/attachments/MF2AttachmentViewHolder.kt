package ru.fireburn.kittybox.ui.create.attachments

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import ru.fireburn.kittybox.micropub.MF2Attachment
import ru.fireburn.kittybox.ui.create.CreatePostActivity

abstract class MF2AttachmentViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    abstract var attachment: MF2Attachment?
    fun onLongPress(v: View) {
        attachment?.let {
            (v.context as CreatePostActivity).attachmentsAdapter.remove(it)
        }
    }
}