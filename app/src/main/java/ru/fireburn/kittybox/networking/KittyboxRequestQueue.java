package ru.fireburn.kittybox.networking;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class KittyboxRequestQueue {
    private static RequestQueue instance;
    public static synchronized RequestQueue getQueue(Context context) {
        if (instance == null) {
            instance = Volley.newRequestQueue(context.getApplicationContext());
        }
        return instance;
    }
}
