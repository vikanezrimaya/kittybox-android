package ru.fireburn.kittybox.networking

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

abstract class MediaUploadBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        // This method is called when the BroadcastReceiver is receiving an Intent broadcast.
        if (intent.action == MediaUploadService.MEDIA_UPLOAD_PROGRESS) {
            intent.extras?.let {
                onProgress(it.getString("id")!!, it.getLong("readBytes"), it.getLong("totalBytes")
                )
            }
        } else if (intent.action == MediaUploadService.MEDIA_UPLOAD_FINISHED) {
            intent.extras?.let {
                onFinished(it.getString("id")!!, it.getString("url")!!)
            }
        }
    }
    abstract fun onProgress(id: String, readBytes: Long, totalBytes: Long)

    abstract fun onFinished(id: String, url: String)
}
