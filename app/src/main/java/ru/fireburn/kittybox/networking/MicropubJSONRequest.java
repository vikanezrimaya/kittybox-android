package ru.fireburn.kittybox.networking;

import androidx.annotation.GuardedBy;
import androidx.annotation.Nullable;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class MicropubJSONRequest extends Request<String> {
    private static final String PROTOCOL_CHARSET = "utf-8";
    private static final String PROTOCOL_CONTENT_TYPE =
            String.format("application/json; charset=%s", PROTOCOL_CHARSET);
    /** Lock to guard mListener as it is cleared on cancel() and read on delivery. */
    private final Object mLock = new Object();
    private final String mToken;

    @Nullable
    @GuardedBy("mLock")
    private Response.Listener<String> mListener;
    private String mData;

    /**
     * Creates a new request.
     *
     * @param url URL to fetch the string at
     * @param bearerToken Token to authenticate with
     * @param data Data to post
     * @param listener Listener to receive the String response
     * @param errorListener Error listener, or null to ignore errors
     */
    public MicropubJSONRequest(
            String url,
            String bearerToken,
            JSONObject data,
            @Nullable Response.Listener<String> listener,
            @Nullable Response.ErrorListener errorListener) {
        super(Method.POST, url, errorListener);
        mToken = bearerToken;
        mListener = listener;
        mData = data.toString();
    }

    @Override
    public String getBodyContentType() {
        return PROTOCOL_CONTENT_TYPE;
    }

    @Override
    public byte[] getBody() {
        try {
            return mData == null ? null : mData.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf(
                    "Unsupported Encoding while trying to get the bytes of %s using %s",
                    mData, PROTOCOL_CHARSET);
            return null;
        }
    }

    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        String location = response.headers.get("Location");
        int statusCode = response.statusCode;
        if (statusCode == 201 || statusCode == 202 && location != null) {
            return Response.success(location, HttpHeaderParser.parseCacheHeaders(response));
        } else {
            try {
                return Response.error(new VolleyError(new String(response.data, HttpHeaderParser.parseCharset(response.headers))));
            } catch (UnsupportedEncodingException e) {
                VolleyLog.wtf(e, "Unsupported Encoding while trying to get the bytes of %s using %s",
                        response.data, PROTOCOL_CHARSET);
                return null;
            }
        }
    }

    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", String.format("Bearer %s", mToken));
        return headers;
    }

    @Override
    protected void deliverResponse(String response) {
        Response.Listener<String> listener;
        synchronized (mLock) {
            listener = mListener;
        }
        if (listener != null) {
            listener.onResponse(response);
        }
    }
}