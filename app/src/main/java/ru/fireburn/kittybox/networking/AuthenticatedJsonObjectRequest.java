package ru.fireburn.kittybox.networking;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AuthenticatedJsonObjectRequest extends JsonObjectRequest {
    private final String token;

    public AuthenticatedJsonObjectRequest(String url, String token, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, null, listener, errorListener);
        this.token = token;
    }

    public AuthenticatedJsonObjectRequest(String url, String token, JSONObject body, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(Method.POST, url, body, listener, errorListener);
        this.token = token;
    }

    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", String.format("Bearer %s", token));
        return headers;
    }
}
