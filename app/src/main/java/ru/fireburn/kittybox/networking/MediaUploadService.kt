package ru.fireburn.kittybox.networking

import android.app.Notification
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import androidx.browser.trusted.NotificationApiHelperForM
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.BlobDataPart
import com.github.kittinunf.fuel.core.Headers
import com.github.kittinunf.fuel.core.extensions.authentication
import ru.fireburn.kittybox.Application
import ru.fireburn.kittybox.R
import java.io.InputStream
import java.util.*
import kotlin.random.Random

class MediaUploadService : Service() {
    companion object Intents {
        const val MEDIA_UPLOAD_PROGRESS = "ru.fireburn.kittybox.MediaUploadProgress"
        const val MEDIA_UPLOAD_FINISHED = "ru.fireburn.kittybox.MediaUploadFinished"
    }
    private val binder = MediaUploadBinder()
    inner class MediaUploadBinder : Binder() {
        fun getService() : MediaUploadService = this@MediaUploadService
    }
    private lateinit var mediaEndpoint: String
    private lateinit var accessToken: String
    override fun onCreate() {
        super.onCreate()
        val sharedPrefs = getSharedPreferences("endpoints", Context.MODE_PRIVATE)
        mediaEndpoint = sharedPrefs.getString("media_endpoint", "https://fireburn.ru/upload_media/")!!
        accessToken = sharedPrefs.getString("access_token", null)!!

    }
    override fun onBind(intent: Intent): IBinder {
        return binder
    }

    fun uploadFile(inputStream: InputStream, size: Long, mime: String?) : String {
        val uploadId = UUID.randomUUID().toString()
        val file = if (mime == null) {
            BlobDataPart(inputStream, name = "file", filename = uploadId, contentLength = size)
        } else {
            BlobDataPart(inputStream, name = "file", filename = uploadId, contentLength = size, contentType = mime)
        }
        val notificationBuilder = NotificationCompat.Builder(this, Application.UPLOAD_SERVICE_NOTIFICATION_CHANNEL)
            .setOngoing(true)
            .setSmallIcon(R.drawable.ic_dashboard_black_24dp)
            .setProgress(0, 0, true)
        val notificationId = Random.nextInt()
        val notificationManager = NotificationManagerCompat.from(this)
        notificationManager.notify(notificationId, notificationBuilder.build())
        var progress = 0
        Fuel.upload(mediaEndpoint)
            .add(file)
            .authentication().bearer(accessToken).requestProgress { readBytes, totalBytes ->
                sendBroadcast(Intent(MEDIA_UPLOAD_PROGRESS)
                    .putExtra("id", uploadId)
                    .putExtra("readBytes", readBytes)
                    .putExtra("totalBytes", totalBytes))
                Log.d("MediaUploadService", "$uploadId: $readBytes/$totalBytes -> ${((readBytes.toDouble()/totalBytes)*100).toInt()}")
                if (progress != ((readBytes.toDouble()/totalBytes)*50).toInt()) {
                    progress = ((readBytes.toDouble()/totalBytes)*50).toInt().also {
                        notificationManager.notify(
                            notificationId,
                            notificationBuilder.setProgress(50, it, false).build()
                        )
                    }
                }
            }.responseString { request, response, result ->
                Log.d("MediaUploadService",request.toString())
                Log.d("MediaUploadService", response.toString())
                Log.d("MediaUploadService",result.toString())

                Log.d("MediaUploadService", "${response.statusCode} (should be 201 or 202), ${response.header(Headers.LOCATION).first()} (should be an URL)")
                Log.d("MediaUploadService", response.headers.toString())

                sendBroadcast(Intent(MEDIA_UPLOAD_FINISHED)
                    .putExtra("id", uploadId)
                    // throws NoSuchElementException: List is empty.
                    .putExtra("url", response.headers[Headers.LOCATION].first()))

                notificationManager.cancel(notificationId)
            }
        return uploadId
    }
}
