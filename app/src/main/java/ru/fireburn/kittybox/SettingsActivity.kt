package ru.fireburn.kittybox

import android.app.Application
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.MultiSelectListPreference
import androidx.preference.PreferenceFragmentCompat
import org.json.JSONArray
import org.json.JSONObject
import ru.fireburn.kittybox.networking.AuthenticatedJsonObjectRequest
import ru.fireburn.kittybox.networking.KittyboxRequestQueue

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings, SettingsFragment())
            .commit()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)
            val syndicationList = findPreference<MultiSelectListPreference>("default_syndication")!!
            syndicationList.isEnabled = false
            val prefs = activity!!.getSharedPreferences("endpoints", Application.MODE_PRIVATE)
            KittyboxRequestQueue.getQueue(activity).add(AuthenticatedJsonObjectRequest(
                Uri.parse(prefs.getString("micropub", null)).buildUpon().appendQueryParameter("q", "config").toString(),
                prefs.getString("access_token", null),
                fun(json) {
                    val syndicateTo: JSONArray = json["syndicate-to"] as JSONArray
                    syndicationList.entries = Array(syndicateTo.length(), fun(int) = (syndicateTo[int] as JSONObject)["name"] as String)
                    syndicationList.entryValues = Array(syndicateTo.length(), fun(int) = (syndicateTo[int] as JSONObject)["uid"] as String)
                    syndicationList.isEnabled = true
                }, null
            ))
        }
    }
}