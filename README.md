# Kittybox Companion for Android
## What is it
This is an Android app serving as a Microsub reader and a Micropub client. It is
intended to have a lot of features, including some experimental properties
posted via Micropub. Use with Kittybox software suite is recommended, though it
will work with any other IndieWeb software, as it tries to conform to protocols
as much as possible, so basic functionality will always work.

## What works
 - Microsub channel listing
 - First page loads
 - Posts render
 - Partial HTML support
 - Likes on posts
 - Syndication
 - Micropub
 
## What doesn't
 - Micropub attachments
 - u-in-reply-to and similar properties are not rendered
 - Microsub beyond first page of a channel
 - IndieAuth (you need to inject credentials manually, may need root)

## Is it any good?
Probably! It's already usable for basic things, like checking if
[@indiewebcat](https://indiewebcat.com) has posted something new on her website,
and mashing the like button once she does. Also for posting an ASCII art of your
kitty maybe.